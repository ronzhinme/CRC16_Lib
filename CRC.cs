﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CRC
{
  public static class CRC
  {
    [DllImport ("CRCcpplib.DLL", EntryPoint = "GetCRC", CallingConvention = CallingConvention.Cdecl)]
    internal static extern uint GetCRC (IntPtr pdata, int data_len, int crc_len, int poly, int init_val, int xor_val, bool invert_data, bool invert_xor, UIntPtr table);


    public static byte[] ComputeCRC (byte[] data, int crc_len, int poly, int init_val, int xor_val, bool invert_data, bool invert_xor)
    {
      if(System.IO.File.Exists("CRCcpplib.dll")==false)
      {
        var dir = AppDomain.CurrentDomain.BaseDirectory;
        return null;
      }
      var dlen = data.Count();
      var dtpntr = Marshal.AllocHGlobal(dlen);
      Marshal.Copy(data,0,dtpntr,dlen);
      var result = GetCRC(dtpntr, dlen, crc_len, poly, init_val, xor_val, invert_data, invert_xor, UIntPtr.Zero );
      var bytes = BitConverter.GetBytes(result);

      return bytes;
    }
  }
}
