#include <Windows.h>
#include "CppUnitTest.h"

#include "../CRCcpplib/CRCcpplib.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace CRC_cpplib_UnitTest
{
#define MAX_ERRMSGLEN 255
	wchar_t *err_msg = new wchar_t[MAX_ERRMSGLEN];
#define SayErrorMessage(A)   {mbstowcs(err_msg,A,MAX_ERRMSGLEN);Assert::Fail(err_msg);}

	TEST_CLASS(UnitTest1)
	{

		typedef unsigned int (GetCRCFunction)(char* data, unsigned long data_len, char crc_bitlen, int poly, int init_val, int xor_val, bool invert_data, bool invert_crc, unsigned int *table);
		typedef bool (MakeTableFunction)(unsigned int *crc_table, char crc_bitlen, int poly);

		GetCRCFunction *getcrc;
		MakeTableFunction *maktbl;

		HMODULE libhndl;
		bool loaded = false;

		bool LoadLibFunctions()
		{
			if (loaded != false)return true;

			libhndl = LoadLibraryA("CRCcpplib.dll");
			if (libhndl == NULL)return false;

			getcrc = (GetCRCFunction*)GetProcAddress(libhndl, "GetCRC");
			if (getcrc == NULL)return false;

			maktbl = (MakeTableFunction*)GetProcAddress(libhndl, "MakeCRCTable");
			if (maktbl == NULL)return false;


			return true;
		}

	public:

		TEST_METHOD(GetCRC16)
		{
			if (!LoadLibFunctions())SayErrorMessage("No lib found or functions");

			char crc_len = 16;
			int test_len = 9;
			char* test = "123456789";

			//CCIIT
			unsigned int correct_crc = 0x29B1;
			unsigned int res = getcrc(test, test_len, crc_len, 0x1021, 0xFFFF, 0x0, 0, 0, NULL);
			if (res != correct_crc)
			{
				char msg[MAX_ERRMSGLEN];
				sprintf(msg, "CRC16_CCIIT need:%X have:%X", correct_crc, res);
				SayErrorMessage(msg);
			}

			//CCIIT + invert_xor
			correct_crc = 0x8D94;
			res = getcrc(test, test_len, crc_len, 0x1021, 0xFFFF, 0x0, 0, 1, NULL);
			if (res != correct_crc)
			{
				char msg[MAX_ERRMSGLEN];
				sprintf(msg, "CRC16_revCRC need:%X have:%X", correct_crc, res);
				SayErrorMessage(msg);
			}

			//CCIIT + invert_xor + xor_value
			correct_crc = 0x4158;
			res = getcrc(test, test_len, crc_len, 0x1021, 0xFFFF, 0xCCCC, 0, 1, NULL);
			if (res != correct_crc)
			{
				char msg[MAX_ERRMSGLEN];
				sprintf(msg, "CRC16_XORrevCRC need:%X have:%X", correct_crc, res);
				SayErrorMessage(msg);
			}

			//CCIIT + invert_xor + xor_value + invert_data;
			correct_crc = 0xA35D;
			res = getcrc(test, test_len, crc_len, 0x1021, 0xFFFF, 0xCCCC, 1, 1, NULL);
			if (res != correct_crc)
			{
				char msg[MAX_ERRMSGLEN];
				sprintf(msg, "CRC16_XORrevCRCDT need:%X have:%X", correct_crc, res);
				SayErrorMessage(msg);
			}

			//IBM
			correct_crc = 0xFEE8;
			res = getcrc(test, test_len, crc_len, 0x8005, 0x0, 0x0, 0, 0, NULL);
			if (res != correct_crc)
			{
				char msg[MAX_ERRMSGLEN];
				sprintf(msg, "CRC16_ibm need:%X have:%X", correct_crc, res);
				SayErrorMessage(msg);
			}
		}

		TEST_METHOD(GetCRC16WithTable)
		{
			if (!LoadLibFunctions())SayErrorMessage("No lib found or functions");

			char crc_len = 16;
			int test_len = 9;
			char* test = "123456789";

			unsigned int table[256];
			memset(&table, 0, 256);



			//CCIIT
			unsigned int correct_crc = 0x29B1;
			maktbl(table, crc_len, 0x1021);
			unsigned int res = getcrc(test, test_len, crc_len, 0x1021, 0xFFFF, 0x0, 0, 0, table);
			if (res != correct_crc)
			{
				char msg[MAX_ERRMSGLEN];
				sprintf(msg, "CRC16_CCIIT need:%X have:%X", correct_crc, res);
				SayErrorMessage(msg);
			}

			//CCIIT + invert_xor
			correct_crc = 0x8D94;
			res = getcrc(test, test_len, crc_len, 0x1021, 0xFFFF, 0x0, 0, 1, table);
			if (res != correct_crc)
			{
				char msg[MAX_ERRMSGLEN];
				sprintf(msg, "CRC16_revCRC need:%X have:%X", correct_crc, res);
				SayErrorMessage(msg);
			}

			//CCIIT + invert_xor + xor_value
			correct_crc = 0x4158;
			res = getcrc(test, test_len, crc_len, 0x1021, 0xFFFF, 0xCCCC, 0, 1, table);
			if (res != correct_crc)
			{
				char msg[MAX_ERRMSGLEN];
				sprintf(msg, "CRC16_XORrevCRC need:%X have:%X", correct_crc, res);
				SayErrorMessage(msg);
			}

			//CCIIT + invert_xor + xor_value + invert_data;
			correct_crc = 0xA35D;
			res = getcrc(test, test_len, crc_len, 0x1021, 0xFFFF, 0xCCCC, 1, 1, table);
			if (res != correct_crc)
			{
				char msg[MAX_ERRMSGLEN];
				sprintf(msg, "CRC16_XORrevCRCDT need:%X have:%X", correct_crc, res);
				SayErrorMessage(msg);
			}

			//IBM
			correct_crc = 0xFEE8;
			maktbl(table, crc_len, 0x8005);
			res = getcrc(test, test_len, crc_len, 0x8005, 0x0, 0x0, 0, 0, table);
			if (res != correct_crc)
			{
				char msg[MAX_ERRMSGLEN];
				sprintf(msg, "CRC16_ibm need:%X have:%X", correct_crc, res);
				SayErrorMessage(msg);
			}
		}
	};
}