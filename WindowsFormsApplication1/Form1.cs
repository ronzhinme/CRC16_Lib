﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CRC;
namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Text = string.Empty;
            var data = new byte[] { 1,2,3,4,5,6,7,8,9,0};
            byte[] crc = CRC.CRC.ComputeCRC(data, 16, 1, 1, 1, false, false);

            foreach (var ch in crc)
            {
               button1.Text += ((int)ch).ToString();
            }
        }
    }
}
