// CRCcpplib.cpp : Defines the exported functions for the DLL application.
//
#include "CRCcpplib.h"
#include <string.h>

unsigned int Reflect(unsigned int val, unsigned bitnum)
{
  unsigned int out=0;
  unsigned int j=val;
  for(unsigned int indx=0;indx<bitnum;++indx)
  {
    unsigned int tmp = 1<<((bitnum-1)-indx);
    if(j&1)
      val|=tmp;
    else
      val&=~tmp;
    j>>=1;
  }
  return val;
}
template <class T> bool _MakeCRCTable(T* crc_table, char crc_bitlen, int poly)
{
  unsigned int r;
  for(T i=0;i<256;i++)
  {
    r=i<<(crc_bitlen-8);
    if(r!=0x0)
    {
      for(int j=0; j<8; j++)
      {
        if(r & (1<<(crc_bitlen-1))) r=(T)(r<<1)^poly;
        else r<<=1;
      }
    }
    crc_table[i]=(T)r;
  }
  return true;
}
template <class T> T _GetCRC(T* crc_table, char* data, int data_len,char crc_bitlen, int init_val, int xor_val,bool invert_data, bool invert_crc)
{
  T crc=(T)init_val;
  for(unsigned int f=0;f<(unsigned int)data_len;++f)
  {
    char p = data[f];
    if(invert_data)p=Reflect(p,8);
    crc = (T)crc_table[((crc>>8)^p)&0xFF] ^ (T)(crc<<8); //здесь 0xFF зависит от длины КС
  }

  if(invert_crc)
  {
    crc = Reflect(crc,crc_bitlen);
  }

  crc=(T)crc^xor_val;
  return (T)crc;
}

bool MakeCRCTable(unsigned int *crc_table, char crc_bitlen, int poly)
{
	return _MakeCRCTable(crc_table, crc_bitlen, poly);
}

unsigned int GetCRCNoTable(char* data, unsigned long data_len, char crc_bitlen, int poly, int init_val, int xor_val,bool invert_data, bool invert_crc)
{
  if(crc_bitlen<=16 && crc_bitlen>=13)
  {
    unsigned short* table = new unsigned short[256];
    memset(table,0,sizeof(unsigned short)*256);

    if(!_MakeCRCTable(table,crc_bitlen,poly))return -1;
	unsigned short crc = _GetCRC(table, data, data_len, crc_bitlen, init_val, xor_val, invert_data, invert_crc);
	delete[] table;
	return crc;
  }
  return 0;
}

unsigned int GetCRC(char* data, unsigned long data_len, char crc_bitlen, int poly, int init_val, int xor_val, bool invert_data, bool invert_crc, unsigned int *crc_table=NULL)
{
	if (crc_bitlen <= 16 && crc_bitlen >= 13)
	{
		if (crc_table != NULL)
		{
			return (unsigned short) _GetCRC(crc_table, data, data_len, crc_bitlen, init_val, xor_val, invert_data, invert_crc);
		}
		else
		{
			return (unsigned short)GetCRCNoTable(data, data_len, crc_bitlen, poly, init_val, xor_val, invert_data, invert_crc);
		}		
	}
	return 0;
}