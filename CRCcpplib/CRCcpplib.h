#define DLLEXPORT extern "C" _declspec(dllexport) 
DLLEXPORT unsigned int GetCRC(char* data, unsigned long data_len, char crc_bitlen, int poly, int init_val, int xor_val, bool invert_data, bool invert_crc, unsigned int *crc_table);
DLLEXPORT bool MakeCRCTable(unsigned int *crc_table, char crc_bitlen, int poly);
